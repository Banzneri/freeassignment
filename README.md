# Free.fi Java Spring Boot assingment

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally

To run the project locally, execute the `main` method in the `com.example.freeassignment.FreeassignmentApplication` class from your IDE.

## Endpoints

Example requests with VSCode Rest Client extension:

```
POST http://localhost:8080/validate_ssn HTTP/1.1
content-type: application/json

{
    "ssn": "210488-125A",
    "countryCode": "fi"
}

GET http://localhost:8080/exchange_amount HTTP/1.1
content-type: application/json

{
    "from": "USD",
    "to": "SEK",
    "fromAmount": 20
}
```

## Disclaimer

I didn't have time to finish the cron job part of the assignment, as I only had a day and a half to finish the assignment after my internship work hours. This was also my first time using Spring Boot, and it's been years since I've touched Java, but all in all, I'm happy with what I achieved in such a short time with a completely new framework.
