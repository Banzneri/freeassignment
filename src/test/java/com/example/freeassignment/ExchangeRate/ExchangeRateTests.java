package com.example.freeassignment.ExchangeRate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ExchangeRateTests {

  private String mapValue(Object obj) throws Exception {
  ObjectMapper mapper = new ObjectMapper();
  return mapper.writeValueAsString(obj);
  }

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;

  @Before
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
  }

  @Test
  public void shouldReturnCorrectUSDToEUR() throws Exception {
    ExchangeInput input = new ExchangeInput("USD", "EUR", 2.0);
    ExchangeOutput output = new ExchangeOutput("USD", "EUR", 1.86, 0.93);

    MvcResult result = this.mockMvc.perform(get("/exchange_amount")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mapValue(input)))
      .andDo(print())
      .andExpect(status().isOk())
      .andReturn();
    assertEquals(mapValue(output), result.getResponse().getContentAsString());
  }

  @Test
  public void shouldReturnCorrectEURToSEK() throws Exception {
    ExchangeInput input = new ExchangeInput("EUR", "SEK", 2.0);
    ExchangeOutput output = new ExchangeOutput("EUR", "SEK", 21.06, 10.53);

    MvcResult result = this.mockMvc.perform(get("/exchange_amount")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mapValue(input)))
      .andDo(print())
      .andExpect(status().isOk())
      .andReturn();
    assertEquals(mapValue(output), result.getResponse().getContentAsString());
  }

  @Test
  public void shouldReturnCorrectSEKToUSD() throws Exception {
    ExchangeInput input = new ExchangeInput("SEK", "USD", 2.0);
    ExchangeOutput output = new ExchangeOutput("SEK", "USD", 0.2, 0.1);

    MvcResult result = this.mockMvc.perform(get("/exchange_amount")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mapValue(input)))
      .andDo(print())
      .andExpect(status().isOk())
      .andReturn();
    assertEquals(mapValue(output), result.getResponse().getContentAsString());
  }

  @Test
  public void shouldReturnOkIfCorrectInput() throws Exception {
    ExchangeInput input = new ExchangeInput("USD", "EUR", Double.valueOf(20.0));

    this.mockMvc.perform(get("/exchange_amount")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mapValue(input)))
      .andDo(print())
      .andExpect(status().isOk());
  }

  @Test
  public void shouldReturnBadRequestIfAnyInputIsNull() throws Exception {
    ExchangeInput input1 = new ExchangeInput(null, "EUR", Double.valueOf(20.0));
    ExchangeInput input2 = new ExchangeInput("USD", null, Double.valueOf(20.0));
    ExchangeInput input3 = new ExchangeInput("USD", "EUR", null);

    String[] inputs = {mapValue(input1), mapValue(input2), mapValue(input3)};

    for (String input : inputs) {
      this.mockMvc.perform(get("/exchange_amount")
        .contentType(MediaType.APPLICATION_JSON)
        .content(input))
        .andDo(print())
        .andExpect(status().isBadRequest());
    }
  }

  public void shouldReturnBadRequestIfWrongCurrency() throws Exception {
    ExchangeInput input = new ExchangeInput("USD", "SKE", Double.valueOf(20.0));

    this.mockMvc.perform(get("/exchange_amount")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mapValue(input)))
      .andDo(print())
      .andExpect(status().isBadRequest());
  }
}
