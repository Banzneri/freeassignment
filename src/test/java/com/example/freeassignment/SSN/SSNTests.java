package com.example.freeassignment.SSN;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SSNTests {

  private String mapValue(Object obj) throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(obj);
  }

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;

  @Before
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
  }

  @Test
  public void shouldReturnFalseIfDateNotNumeric() throws Exception {
    SSNInput input = new SSNInput("21k588-125A", "fi");
    String mappedInput = mapValue(input);
    String expectedOutput = mapValue(new SSNOutput(false));

    MvcResult result = this.mockMvc.perform(post("/validate_ssn")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mappedInput))
      .andDo(print())
      .andExpect(status().isOk())
      .andReturn();
    assertEquals(expectedOutput, result.getResponse().getContentAsString());
  }

  @Test
  public void shouldReturnTrueIfDateNumeric() throws Exception {
    SSNInput input = new SSNInput("210588-125A", "fi");
    String mappedInput = mapValue(input);
    String expectedOutput = mapValue(new SSNOutput(true));

    MvcResult result = this.mockMvc.perform(post("/validate_ssn")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mappedInput))
      .andDo(print())
      .andExpect(status().isOk())
      .andReturn();
    assertEquals(expectedOutput, result.getResponse().getContentAsString());
  }

  @Test
  public void shouldReturnFalseIfInvalidDay() throws Exception {
    SSNInput input = new SSNInput("440588-125A", "fi");
    String mappedInput = mapValue(input);
    String expectedOutput = mapValue(new SSNOutput(false));

    MvcResult result = this.mockMvc.perform(post("/validate_ssn")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mappedInput))
      .andDo(print())
      .andExpect(status().isOk())
      .andReturn();
    assertEquals(expectedOutput, result.getResponse().getContentAsString());
  }

  @Test
  public void shouldReturnTrueBecauseRealIDs() throws Exception {
    SSNInput input = new SSNInput("020791-126F", "fi");
    SSNInput input2 = new SSNInput("210588-125A", "fi");
    String mappedInput = mapValue(input);
    String mappedInput2 = mapValue(input2);
    String expectedOutput = mapValue(new SSNOutput(true));
    String expectedOutput2 = mapValue(new SSNOutput(true));

    MvcResult result = this.mockMvc.perform(post("/validate_ssn")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mappedInput))
      .andDo(print())
      .andExpect(status().isOk())
      .andReturn();
    MvcResult result2 = this.mockMvc.perform(post("/validate_ssn")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mappedInput2))
      .andDo(print())
      .andExpect(status().isOk())
      .andReturn();
    assertEquals(expectedOutput, result.getResponse().getContentAsString());
    assertEquals(expectedOutput2, result2.getResponse().getContentAsString());
  }

  @Test
  public void shouldReturnFalseIfInvalidMonth() throws Exception {
    SSNInput input = new SSNInput("271388-125A", "fi");
    String mappedInput = mapValue(input);
    String expectedOutput = mapValue(new SSNOutput(false));

    MvcResult result = this.mockMvc.perform(post("/validate_ssn")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mappedInput))
      .andDo(print())
      .andExpect(status().isOk())
      .andReturn();
    assertEquals(expectedOutput, result.getResponse().getContentAsString());
  }

  @Test
  public void shouldReturnBadRequestIfSSNNull() throws Exception {
    SSNInput input = new SSNInput(null, "fi");
    String mappedInput = mapValue(input);

    this.mockMvc.perform(post("/validate_ssn")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mappedInput))
      .andDo(print())
      .andExpect(status().isBadRequest());
  }

  @Test
  public void shouldReturnBadRequestIfCountryCodeNull() throws Exception {
    SSNInput input = new SSNInput("210588-125A", null);
    String mappedInput = mapValue(input);

    this.mockMvc.perform(post("/validate_ssn")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mappedInput))
      .andDo(print())
      .andExpect(status().isBadRequest());
  }

  @Test
  public void shouldReturnFalseIfInvalidSign() throws Exception {
    SSNInput input = new SSNInput("210588y125A", "fi");
    String mappedInput = mapValue(input);
    String expectedOutput = mapValue(new SSNOutput(false));

    MvcResult result = this.mockMvc.perform(post("/validate_ssn")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mappedInput))
      .andDo(print())
      .andExpect(status().isOk())
      .andReturn();
    assertEquals(expectedOutput, result.getResponse().getContentAsString());
  }

  @Test
  public void shouldReturnFalseIfInvalidControlCharacter() throws Exception {
    SSNInput input = new SSNInput("210588-125B", "fi");
    String mappedInput = mapValue(input);
    String expectedOutput = mapValue(new SSNOutput(false));

    MvcResult result = this.mockMvc.perform(post("/validate_ssn")
      .contentType(MediaType.APPLICATION_JSON)
      .content(mappedInput))
      .andDo(print())
      .andExpect(status().isOk())
      .andReturn();
    assertEquals(expectedOutput, result.getResponse().getContentAsString());
  }
}