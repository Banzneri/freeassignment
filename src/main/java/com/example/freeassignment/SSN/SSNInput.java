package com.example.freeassignment.SSN;

public class SSNInput {
  String ssn;
  String countryCode;

  SSNInput(String ssn, String countryCode) {
    this.ssn = ssn;
    this.countryCode = countryCode;
  }

  public String getSsn() {
    return ssn;
  }

  public void setSsn(String ssn) {
    this.ssn = ssn;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }
}
