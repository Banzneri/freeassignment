package com.example.freeassignment.SSN;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SSNController {
  SSNController() { }
  
  @PostMapping(value = "/validate_ssn", produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<SSNOutput> validateSSN(@RequestBody SSNInput ssnInput) {
    SSNValidator ssnValidator = new SSNValidator();

    if (!ssnValidator.isValid(ssnInput)) {
      return ResponseEntity.ok(new SSNOutput(false));
    }

    return ResponseEntity.ok(new SSNOutput(true));
  }
}
