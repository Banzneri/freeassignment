package com.example.freeassignment.SSN;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class SSNBadRequestAdvice {
  @ResponseBody
  @ExceptionHandler(SSNBadRequestException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  String SSNBadRequestHandler(SSNBadRequestException ex) {
    return ex.getMessage();
  }
}
