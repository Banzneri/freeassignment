package com.example.freeassignment.SSN;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class SSNValidator {
  public SSNValidator() {}

  private boolean isNumeric(String input) {
    try {
      Integer.parseInt(input);
    } catch (Exception e) {
      return false;
    }
    return true;
  }

  private boolean isValidIndividualNumber(String input) {
    if (!isNumeric(input)) {
      return false;
    }

    int number = Integer.parseInt(input);

    if (number > 899 || number < 2) {
      return false;
    }

    return true;
  }

  private boolean isValidDate(String dateString) {
    String dateFormat = "ddMMyy";
    DateFormat sdf = new SimpleDateFormat(dateFormat);
    sdf.setLenient(false);

    try {
      sdf.parse(dateString);
    } catch (Exception e) {
      return false;
    }

    return true;
  }

  private boolean isValidMiddleSign(char sign) {
    String signs = "+-a";
    String formattedSign = Character.toString(sign).toLowerCase();
    return signs.contains(formattedSign);
  }

  private boolean isValidControlCharacter(char input, int digits) {
    String[] controlTable = {
      "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
      "A", "B", "C", "D", "E", "F", "H", "J", "K", "L", "M",
      "N", "P", "R", "S", "T", "U", "V", "W", "X", "Y"};

    String control = Character.toString(input).toUpperCase();
    double remainder = digits % 31;
    int integerPart = (int) remainder;
    double decimalPart = remainder - integerPart;

    if (decimalPart != 0) {
      int index = (int) Math.round(decimalPart * 31);
      return controlTable[index].equals(control);
    }

    return controlTable[integerPart].equals(control);
  }

  public boolean isValid(SSNInput input) {
    if (input.ssn == null || input.countryCode == null) {
      throw new SSNBadRequestException();
    }

    if (input.ssn.length() > 11) {
      return false;
    }

    String date = input.ssn.substring(0, 6);

    if (!isValidDate(date)) {
      return false;
    }

    char middleSign = input.ssn.charAt(6);

    if (!isValidMiddleSign(middleSign)) {
      return false;
    }

    String individualNumber = input.ssn.substring(7, 10);

    if (!isValidIndividualNumber(individualNumber)) {
      return false;
    }

    char controlCharacter = input.ssn.charAt(10);
    int digits = Integer.parseInt(date + individualNumber);

    if (!isValidControlCharacter(controlCharacter, digits)) {
      return false;
    }

    return true;
  }
}
