package com.example.freeassignment.SSN;

public class SSNOutput {
  Boolean ssnValid;

  public Boolean getSsnValid() {
    return ssnValid;
  }

  public void setSsnValid(Boolean ssnValid) {
    this.ssnValid = ssnValid;
  }

  SSNOutput(Boolean ssnValid) {
    this.ssnValid = ssnValid;
  }
}
