package com.example.freeassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FreeassignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(FreeassignmentApplication.class, args);
	}

}
