package com.example.freeassignment.ExchangeRate;

public class ExchangeInput {
  String from;
  String to;
  Double fromAmount;

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public Double getFromAmount() {
    return fromAmount;
  }

  public void setFromAmount(Double fromAmount) {
    this.fromAmount = fromAmount;
  }

  ExchangeInput(String from, String to, Double fromAmount) {
    this.from = from;
    this.to = to;
    this.fromAmount = fromAmount;
  }
}
