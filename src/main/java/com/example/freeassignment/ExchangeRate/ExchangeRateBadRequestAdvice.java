package com.example.freeassignment.ExchangeRate;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExchangeRateBadRequestAdvice {
  @ResponseBody
  @ExceptionHandler(ExchangeRateBadRequestException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  String exchangeRateBadRequestHandler(ExchangeRateBadRequestException ex) {
    return ex.getMessage();
  }
}
