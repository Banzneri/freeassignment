package com.example.freeassignment.ExchangeRate;

public class ExchangeRateValidator {
  ExchangeRateValidator() { }

  public boolean isValid(ExchangeInput input) {
    if (input.to == null || input.from == null || input.fromAmount == null) {
      return false;
    }

    if (!ExchangeRateService.getCurrencies().contains(input.from.toUpperCase())) {
      return false;
    }

    if (!ExchangeRateService.getCurrencies().contains(input.to.toUpperCase())) {
      return false;
    }

    return true;
  }
}
