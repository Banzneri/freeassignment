package com.example.freeassignment.ExchangeRate;

public class ExchangeOutput {
  String from;
  String to;
  Double toAmount;
  Double exchangeRate;

  ExchangeOutput(String from, String to, Double toAmount, Double exchangeRate) {
    this.from = from;
    this.to = to;
    this.toAmount = toAmount;
    this.exchangeRate = exchangeRate;
  }
  
  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }
  
  public Double getToAmount() {
    return toAmount;
  }

  public void setToAmount(Double toAmount) {
    this.toAmount = toAmount;
  }

  public Double getExchangeRate() {
    return exchangeRate;
  }

  public void setExchangeRate(Double exchangeRate) {
    this.exchangeRate = exchangeRate;
  }
}
