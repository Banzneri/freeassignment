package com.example.freeassignment.ExchangeRate;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ExchangeRateService {
  String fromCurrency;
  String toCurrency;

  final Map<String, Double> usdRates = Map.ofEntries(
    Map.entry("SEK", 9.83),
    Map.entry("EUR", 0.93)
  );

  final Map<String, Double> eurRates = Map.ofEntries(
    Map.entry("USD", 1.07),
    Map.entry("SEK", 10.53)
  );

  final Map<String, Double> sekRates = Map.ofEntries(
    Map.entry("USD", 0.10),
    Map.entry("EUR", 0.095)
  );

  final Map<String, Map<String, Double>> rates = Map.ofEntries(
    Map.entry("USD", usdRates),
    Map.entry("EUR", eurRates),
    Map.entry("SEK", sekRates)
  );

  ExchangeRateService(String currency, String toCurrency) {
    this.fromCurrency = currency;
    this.toCurrency = toCurrency;
  }

  public String getCurrency() {
    return fromCurrency;
  }

  public void setCurrency(String currency) {
    this.fromCurrency = currency;
  }

  public String getToCurrency() {
    return toCurrency;
  }

  public void setToCurrency(String toCurrency) {
    this.toCurrency = toCurrency;
  }

  public static List<String> getCurrencies() {
    String[] currencies = {"USD", "SEK", "EUR"};
    return Arrays.asList(currencies);
  }

  public double getRate() {
    return rates.get(fromCurrency).get(toCurrency);
  }

  public double getAmount(double amount) {
    return getRate() * amount;
  }
}
