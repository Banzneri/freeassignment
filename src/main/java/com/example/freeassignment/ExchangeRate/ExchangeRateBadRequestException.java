package com.example.freeassignment.ExchangeRate;

public class ExchangeRateBadRequestException extends RuntimeException {
  ExchangeRateBadRequestException() {
    super("Wrong input");
  }
}
