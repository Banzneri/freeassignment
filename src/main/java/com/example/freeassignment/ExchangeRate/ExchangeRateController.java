package com.example.freeassignment.ExchangeRate;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExchangeRateController {
  ExchangeRateController() { }

  @GetMapping(value = "/exchange_amount", produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<ExchangeOutput> getExchangeRate(@RequestBody ExchangeInput exchangeInput) {
    ExchangeRateValidator validator = new ExchangeRateValidator();

    if (!validator.isValid(exchangeInput)) {
      throw new ExchangeRateBadRequestException();
    }

    String from = exchangeInput.from;
    String to = exchangeInput.to;
    double amount = exchangeInput.fromAmount;

    ExchangeRateService service = new ExchangeRateService(from, to);
    double rateAmount = service.getAmount(amount);
    double rate = service.getRate();
    ExchangeOutput output = new ExchangeOutput(from, to, rateAmount, rate);

    return ResponseEntity.ok(output);
  }
}
